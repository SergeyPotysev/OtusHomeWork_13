//************************************************************
// Piter Gavrinev 2008 piter@mail.cbr.ru
// ������� ������ ������������� ���-������� ��� ��������� ������ �����
// � ������� ������������ ����� �� 
// http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx
//************************************************************

using CurrencyQuery.Models;
using CurrencyQuery.Repository;
using CurrencyQuery.Setting;
using CurrencyQuery.Repository.ADO;
using CurrencyQuery.Repository.EF;
using CurrencyQuery.Elasticsearch;
using System;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using System.Globalization;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates; //for ssl use
using System.Collections.Generic;
using CurrencyQuery.Service;

namespace CurrencyQuery
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MainForm : System.Windows.Forms.Form
	{
        #region �� OTUS

        /// <summary>
        /// ��������� �� ��������� �� ����� appsettings.json
        /// </summary>
        private Settings _settings;

        /// <summary>
        /// ������� ������� �������� �����������
        /// </summary>
        private UsingRepo _ado, _ef, _activeRepo;

        /// <summary>
        /// ������� ����� ��� ������ ����� ������� ����� 
        /// </summary>
        private DataSet val_ds;

        /// <summary>
        /// ������ ������ �����, ���������� � ������� ��
        /// </summary>
        private List<CursDinamic> _cbrCursDinamic;

        /// <summary>
        /// ������ �����, ���������� � ������� ��
        /// </summary>
        private List<Currency> _cbrCurrencyList;

        /// <summary>
        /// ������ Elasticsearch
        /// </summary>
        private Elastic _elastic;

        /// <summary>
        /// ���� "������������ Redis" ��� ������ � ��
        /// </summary>
        private bool _redisUsed;

        /// <summary>
        /// ���� ������������ ���������� (EF/ADO.NET) ��� ������ � ��
        /// </summary>
        private string _technology;

        /// <summary>
        /// ��������� �������� label ����� �� ������� ������.
        /// </summary>
        public delegate void FormWriter(int n, int count);
        private FormWriter _sender;

        #endregion

        private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.DateTimePicker dateTimePicker2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private string Val_code;
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.Label label4;
		public DailyInfoServ.DailyInfo di;
        private Button button3;
        private GroupBox groupBox2;
        private Button OpenTableButton;
        private ComboBox TablesComboBox;
        private GroupBox groupBox1;
        private Button SaveCurListButton;
        private Button SaveDateCursButton;
        private Button ChartFormButton;
        private Label label5;
        private RadioButton EfRadio;
        private RadioButton AdoRadio;
        private Button ExitButton;
        private Button ClearTablesButton;
        private Label RedisLabel;
        private Label label6;
        private Button TestRunButton;
        private Label label7;
        private Label StrNumLabel;
        private Button TestResultButton;
        private CredentialCache myCache;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            #region �� OTUS
            InitRepository();
            #endregion
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.OpenTableButton = new System.Windows.Forms.Button();
            this.TablesComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SaveCurListButton = new System.Windows.Forms.Button();
            this.SaveDateCursButton = new System.Windows.Forms.Button();
            this.ChartFormButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.EfRadio = new System.Windows.Forms.RadioButton();
            this.AdoRadio = new System.Windows.Forms.RadioButton();
            this.ExitButton = new System.Windows.Forms.Button();
            this.ClearTablesButton = new System.Windows.Forms.Button();
            this.RedisLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TestRunButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.StrNumLabel = new System.Windows.Forms.Label();
            this.TestResultButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Location = new System.Drawing.Point(24, 53);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(488, 69);
            this.listBox1.TabIndex = 0;
            this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "ConnectToServer";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(56, 128);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(312, 128);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "�";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(264, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "��";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(24, 184);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 32);
            this.button2.TabIndex = 9;
            this.button2.Text = "�������� ����";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGrid1
            // 
            this.dataGrid1.DataMember = "";
            this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid1.Location = new System.Drawing.Point(24, 232);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.ReadOnly = true;
            this.dataGrid1.Size = new System.Drawing.Size(488, 128);
            this.dataGrid1.TabIndex = 10;
            this.dataGrid1.Navigate += new System.Windows.Forms.NavigateEventHandler(this.dataGrid1_Navigate);
            this.dataGrid1.DoubleClick += new System.EventHandler(this.dataGrid1_DoubleClick);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(488, 16);
            this.label3.TabIndex = 11;
            // 
            // checkBox1
            // 
            this.checkBox1.Location = new System.Drawing.Point(264, 193);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(240, 16);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "������, ��������������� ����������";
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // statusBar1
            // 
            this.statusBar1.Location = new System.Drawing.Point(0, 365);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Size = new System.Drawing.Size(895, 32);
            this.statusBar1.TabIndex = 13;
            this.statusBar1.Text = "statusBar1";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(264, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "������ �����";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(133, 186);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 28);
            this.button3.TabIndex = 15;
            this.button3.Text = "��� xml";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.OpenTableButton);
            this.groupBox2.Controls.Add(this.TablesComboBox);
            this.groupBox2.Location = new System.Drawing.Point(730, 91);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(134, 74);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "�������";
            // 
            // OpenTableButton
            // 
            this.OpenTableButton.Location = new System.Drawing.Point(6, 41);
            this.OpenTableButton.Name = "OpenTableButton";
            this.OpenTableButton.Size = new System.Drawing.Size(121, 23);
            this.OpenTableButton.TabIndex = 24;
            this.OpenTableButton.Text = "����������";
            this.OpenTableButton.UseVisualStyleBackColor = true;
            this.OpenTableButton.Click += new System.EventHandler(this.OpenTableButton_Click);
            // 
            // TablesComboBox
            // 
            this.TablesComboBox.FormattingEnabled = true;
            this.TablesComboBox.Location = new System.Drawing.Point(6, 16);
            this.TablesComboBox.Name = "TablesComboBox";
            this.TablesComboBox.Size = new System.Drawing.Size(121, 21);
            this.TablesComboBox.TabIndex = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SaveCurListButton);
            this.groupBox1.Controls.Add(this.SaveDateCursButton);
            this.groupBox1.Location = new System.Drawing.Point(557, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(134, 74);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� � ��";
            // 
            // SaveCurListButton
            // 
            this.SaveCurListButton.Location = new System.Drawing.Point(7, 14);
            this.SaveCurListButton.Name = "SaveCurListButton";
            this.SaveCurListButton.Size = new System.Drawing.Size(121, 23);
            this.SaveCurListButton.TabIndex = 20;
            this.SaveCurListButton.Text = "������ �����";
            this.SaveCurListButton.UseVisualStyleBackColor = true;
            this.SaveCurListButton.Click += new System.EventHandler(this.SaveCurListButton_Click);
            // 
            // SaveDateCursButton
            // 
            this.SaveDateCursButton.Location = new System.Drawing.Point(7, 41);
            this.SaveDateCursButton.Name = "SaveDateCursButton";
            this.SaveDateCursButton.Size = new System.Drawing.Size(121, 23);
            this.SaveDateCursButton.TabIndex = 21;
            this.SaveDateCursButton.Text = "���� ������";
            this.SaveDateCursButton.UseVisualStyleBackColor = true;
            this.SaveDateCursButton.Click += new System.EventHandler(this.SaveDateCursButton_Click);
            // 
            // ChartFormButton
            // 
            this.ChartFormButton.Location = new System.Drawing.Point(564, 171);
            this.ChartFormButton.Name = "ChartFormButton";
            this.ChartFormButton.Size = new System.Drawing.Size(121, 23);
            this.ChartFormButton.TabIndex = 34;
            this.ChartFormButton.Text = "������ �����";
            this.ChartFormButton.UseVisualStyleBackColor = true;
            this.ChartFormButton.Click += new System.EventHandler(this.ChartFormButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(564, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "������ ������ � ��";
            // 
            // EfRadio
            // 
            this.EfRadio.AutoSize = true;
            this.EfRadio.Location = new System.Drawing.Point(579, 53);
            this.EfRadio.Name = "EfRadio";
            this.EfRadio.Size = new System.Drawing.Size(106, 17);
            this.EfRadio.TabIndex = 32;
            this.EfRadio.TabStop = true;
            this.EfRadio.Text = "Entity Framework";
            this.EfRadio.UseVisualStyleBackColor = true;
            this.EfRadio.Click += new System.EventHandler(this.EfRadio_Click);
            // 
            // AdoRadio
            // 
            this.AdoRadio.AutoSize = true;
            this.AdoRadio.Location = new System.Drawing.Point(579, 30);
            this.AdoRadio.Name = "AdoRadio";
            this.AdoRadio.Size = new System.Drawing.Size(73, 17);
            this.AdoRadio.TabIndex = 31;
            this.AdoRadio.TabStop = true;
            this.AdoRadio.Text = "ADO.NET";
            this.AdoRadio.UseVisualStyleBackColor = true;
            this.AdoRadio.Click += new System.EventHandler(this.AdoRadio_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(808, 336);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 23);
            this.ExitButton.TabIndex = 30;
            this.ExitButton.Text = "�����";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // ClearTablesButton
            // 
            this.ClearTablesButton.Location = new System.Drawing.Point(736, 171);
            this.ClearTablesButton.Name = "ClearTablesButton";
            this.ClearTablesButton.Size = new System.Drawing.Size(121, 23);
            this.ClearTablesButton.TabIndex = 39;
            this.ClearTablesButton.Text = "�������� �������";
            this.ClearTablesButton.UseVisualStyleBackColor = true;
            this.ClearTablesButton.Click += new System.EventHandler(this.ClearTablesButton_Click);
            // 
            // RedisLabel
            // 
            this.RedisLabel.AutoSize = true;
            this.RedisLabel.Location = new System.Drawing.Point(770, 14);
            this.RedisLabel.Name = "RedisLabel";
            this.RedisLabel.Size = new System.Drawing.Size(10, 13);
            this.RedisLabel.TabIndex = 40;
            this.RedisLabel.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(727, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Redis:";
            // 
            // TestRunButton
            // 
            this.TestRunButton.Location = new System.Drawing.Point(564, 263);
            this.TestRunButton.Name = "TestRunButton";
            this.TestRunButton.Size = new System.Drawing.Size(121, 23);
            this.TestRunButton.TabIndex = 42;
            this.TestRunButton.Text = "����";
            this.TestRunButton.UseVisualStyleBackColor = true;
            this.TestRunButton.Click += new System.EventHandler(this.TestRunButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(564, 289);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "������";
            // 
            // StrNumLabel
            // 
            this.StrNumLabel.AutoSize = true;
            this.StrNumLabel.Location = new System.Drawing.Point(613, 289);
            this.StrNumLabel.Name = "StrNumLabel";
            this.StrNumLabel.Size = new System.Drawing.Size(10, 13);
            this.StrNumLabel.TabIndex = 44;
            this.StrNumLabel.Text = "-";
            // 
            // TestResultButton
            // 
            this.TestResultButton.Location = new System.Drawing.Point(736, 263);
            this.TestResultButton.Name = "TestResultButton";
            this.TestResultButton.Size = new System.Drawing.Size(121, 23);
            this.TestResultButton.TabIndex = 45;
            this.TestResultButton.Text = "��������� �����";
            this.TestResultButton.UseVisualStyleBackColor = true;
            this.TestResultButton.Click += new System.EventHandler(this.TestResultButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(895, 397);
            this.Controls.Add(this.TestResultButton);
            this.Controls.Add(this.StrNumLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TestRunButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.RedisLabel);
            this.Controls.Add(this.ClearTablesButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ChartFormButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.EfRadio);
            this.Controls.Add(this.AdoRadio);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.statusBar1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGrid1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion


        public static bool TrustAllCertificateCallback(object sender,
        X509Certificate cert, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        } 

		private void button1_Click(object sender, System.EventArgs e)
		{
            try
            {
                int i;
                System.Data.DataSet my_dataset;
                System.Data.DataRow row;

                string v_name, v_id;
                IWebProxy I_DefWP=null;
                if (di == null)
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback (TrustAllCertificateCallback);

                    I_DefWP = WebRequest.DefaultWebProxy; // ���� ������������ web-������ �� ���������
                    
                    
                    if ((I_DefWP != null) && (I_DefWP.Credentials==null))
                    {
                        
                        Uri uriDest = I_DefWP.GetProxy(new Uri(CurrencyQuery.Properties.Settings.Default.CurrencyQuery_DailyInfoServ_DailyInfo));
                       
                        s_Config sc = new s_Config();
                        QW_ConfData mw = sc.LoadData(); // �������� ������ �� ����� ������������ ���� ��� ����
                        DialogResult res;
                        ProxyParam pp;
                        pp = new ProxyParam();
                        pp.mw_edt = mw;
                        if (mw.PWD == null)
                        {
                            res = pp.ShowDialog();
                        }
                        else
                        {
                            res = DialogResult.OK;
                        }
                        if (res == DialogResult.OK)
                        {
                            mw = pp.mw_edt;
                            sc.SaveData(mw); // ��������� ������ � �����

                            if ((mw.AuthType.ToLower() == "basic"))
                            {
                                // ��� Basic
                                I_DefWP.Credentials = new System.Net.NetworkCredential(mw.UserName, mw.PWD);
                            }
                            else
                            {
                                // ��� ��������� �����
                                NetworkCredential myCred = new NetworkCredential(mw.UserName, mw.PWD, mw.Domain);
                                myCache = new CredentialCache();
                                myCache.Add(uriDest, mw.AuthType, myCred);
                                I_DefWP.Credentials = myCache;
                            }
                            statusBar1.Text = "try connect via proxy (auth mode)... " + uriDest.AbsoluteUri;
                        }
                        else
                        {
                            statusBar1.Text = "try connect via proxy (not auth mode)... " + uriDest.AbsoluteUri;
                        }
                    }
                    else
                    {
                        statusBar1.Text = "try connect ...";
                    }
                    di = new DailyInfoServ.DailyInfo();
                   
                    if (I_DefWP != null)
                    {
                        di.AllowAutoRedirect = true;
                        di.PreAuthenticate = true;
                        di.Proxy = I_DefWP;
                    }             
                }              
                // �������� ������ ����� � DataSet
                my_dataset = (System.Data.DataSet)di.EnumValutes(checkBox1.Checked); //�������� ������ �����
                statusBar1.Text = "�������� ��������� ���� ���������. ������";
                System.Data.DataTable tbl = my_dataset.Tables["EnumValutes"];
                listBox1.BeginUpdate();
                ArrayList ValutesNames = new ArrayList();
                #region �� OTUS
                _cbrCurrencyList = new List<Currency>();
                #endregion
                // ��������� ������(listBox1) �������� ����������� � �������
                for (i = 0; i < tbl.Rows.Count; i++)
                {
                    row = tbl.Rows[i];
                    v_id = (string)row.ItemArray.GetValue(0);
                    v_id = v_id.Trim();
                    v_name = (string)row.ItemArray.GetValue(1);
                    v_name = v_name.Trim();
                    ValutesNames.Add(new Valute(v_name, v_id));
                    #region �� OTUS
                    var cells = row.ItemArray;
                    _cbrCurrencyList.Add(new Currency
                    {
                        Code = cells[0].ToString().Trim(),
                        RusName = cells[1].ToString().Trim(),
                        EngName = cells[2].ToString().Trim(),
                        Nominal = (decimal)cells[3],
                        CommonCode = cells[4].ToString().Trim(),
                        NumCode = (cells[5] != DBNull.Value) ? Convert.ToInt32(cells[5]) : default,
                        CharCode = (cells[6] != DBNull.Value) ? cells[6].ToString().Trim() : default
                    });
                    #endregion
                }
                listBox1.DataSource = ValutesNames;
                listBox1.DisplayMember = "ValuteName";
                listBox1.ValueMember = "ValuteCode";
                listBox1.EndUpdate();

                System.DateTime dtm;
                if (!checkBox1.Checked)
                {
                    dtm = di.GetLatestDateTime();
                    listBox1.SelectedValue = "R01235"; // ������������� ������ ������ ��� ��� ������ �� ���������
                }
                else
                {
                    dtm = di.GetLatestDateTimeSeld();
                }
                dateTimePicker2.MaxDate = dtm;
                dateTimePicker2.Value = dtm;
                button2.Enabled = true;
                button3.Enabled = true;
                statusBar1.Text = "��� ������ ��������, ������ ������� '�������� ����'.";
                #region �� OTUS
                _elastic.AddLogRecord("������� ������ ����� � ������� �� ��", _technology, _redisUsed);
                #endregion
            }
            catch (Exception ex)
            {
                string err_msg = "������: " + ex.Message;
                MessageBox.Show(err_msg, "������");
                statusBar1.Text = err_msg;

            }
		}


		private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (listBox1.SelectedIndex != -1){

				Val_code=listBox1.SelectedValue.ToString();
				label3.Text=listBox1.SelectedItem.ToString();

				}
		}
	

        // ������ "�������� ����"
		private void button2_Click(object sender, System.EventArgs e)
		{
            #region �� OTUS
            if (_activeRepo.GetCurrencyCount() == 0)
            {
                string logText = "�� �������� � �� ������ �����!";
                _elastic.AddLogRecord(logText, _technology, _redisUsed);
                MessageBox.Show(logText, "�������� ��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            #endregion
            System.DateTime  DateFrom,DateTo;
			DateFrom=dateTimePicker1.Value;
			DateTo=dateTimePicker2.Value;
			if (Val_code!="")
			{
			    //�������� GetCursDynamic ��� ��������� ������� � ������� �������� ������
			    statusBar1.Text="������� �������� ����.";
			    val_ds=(System.Data.DataSet)di.GetCursDynamic(DateFrom,DateTo,Val_code);
			    statusBar1.Text="���� ������ �������, �������� ��� ���� �� ����� ������� ��� ��������� ������ ����� �� ����";
			    val_ds.Tables[0].Columns[0].ColumnName="����";
			    val_ds.Tables[0].Columns[1].ColumnName="��.��� ������";
			    val_ds.Tables[0].Columns[2].ColumnName="�������";
			    val_ds.Tables[0].Columns[3].ColumnName="����";
			
			    dataGrid1.SetDataBinding(val_ds,"ValuteCursDynamic");
		        dataGrid1.Enabled=true;

                #region �� OTUS
                _cbrCursDinamic?.Clear();
                _cbrCursDinamic = new List<CursDinamic>();
                string CurrencyCode;
                foreach (DataTable tb in val_ds.Tables)
                {
                    foreach (DataRow row in tb.Rows)
                    {
                        var cells = row.ItemArray;
                        CurrencyCode = cells[1].ToString();
                        _cbrCursDinamic.Add(new CursDinamic
                        {
                            OnDate = (DateTime)cells[0],
                            CurrencyId = _activeRepo.GetCurrencyId(cells[1].ToString()),
                            Curs = (decimal)cells[3]
                        });
                    }
                }
                string currencyName = _activeRepo.GetCurrencyName(Val_code);
                _elastic.AddLogRecord($"�������� {_cbrCurrencyList.Count} ������� ��� ����� ������ {Val_code} ({currencyName})", _technology, _redisUsed);
                #endregion
            }
        }

		private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (di!=null)
			{
			button1.PerformClick();
			dataGrid1.Enabled=false;
			}
		}
	

		private void dataGrid1_DoubleClick(object sender, System.EventArgs e)
		{
			System.Data.DataSet ds_grid;
			System.Data.DataRow row;
			int row_index;
			row_index=dataGrid1.CurrentRowIndex;
			if (row_index>-1)
			{
				ds_grid=(System.Data.DataSet)dataGrid1.DataSource;
				DailyForm df=new DailyForm();
				df.mdi=di;
				df.is_seld=checkBox1.Checked;
				row=ds_grid.Tables[0].Rows[row_index];
				
				df.selected_date=(System.DateTime)row[0];
				df.Text="����� ����� �� "+((System.DateTime)row[0]).ToString("dd.MM.yyyy",DateTimeFormatInfo.InvariantInfo);
				df.ShowDialog(this);
				df.Dispose();
			}

		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			statusBar1.Text="������� ConnectToServer ��� ���������� � ���-��������";

            #region �� OTUS
            this.Text = "                      ���-������ ��� ��������� ������ ����� � ������� �����������" +
"� ����� ��  <- | -> �� OTUS � ������� � 13";
            // ������ ������
            TablesComboBox.Items.AddRange(new object[] 
            {
                "������ �����",
                "�������� �����",
                "�������",
                "�����",
                "����"
            });
            // �������� ������� - Currencies (������ �����)
            TablesComboBox.SelectedIndex = 0;
            _elastic.AddLogRecord("������ ������", _technology, _redisUsed);
            // ���� ������ ����� ��� ����������, ��������� �������� �� �����
            SaveCurListButton.Enabled = _activeRepo.GetCurrencyCount() == 0;
            // ���� ����� ����������� �����, ����� �������� ������
            TestResultButton.Enabled = _activeRepo.GetTimingCount() > 0;
            // ���� ���� ������ ��� ������, ����� �������� ������ �����. ������ 5 ����������� ����� �������������, �.�. �������� � ~ 100 ��� �� ���������� ����
            TestRunButton.Enabled = _activeRepo.GetCursDinamicCount() > 5;     
            #endregion
        }


        private void dataGrid1_Navigate(object sender, NavigateEventArgs ne)
        {

        }


        private void button3_Click(object sender, EventArgs e)
        {
            // �������� ������ � �������� ����� � ���� �������� XML
			System.DateTime  DateFrom,DateTo;
			DateFrom=dateTimePicker1.Value;
			DateTo=dateTimePicker2.Value;
            if (Val_code != "")
            {
               System.Xml.XmlNode ret_xml = di.GetCursDynamicXML(DateFrom,DateTo,Val_code);
               MessageBox.Show(ret_xml.InnerXml, "Simple XML (GetCursDynamicXML)");               
            }
        }


        #region �� OTUS
        private void InitRepository()
        {
            _settings = Settings.GetAppJson();
            _ado = new UsingRepo(new ADORepository());
            _ef = new UsingRepo(new EFRepository());
            // ���� ��� ������ ������, �� ������� ������� ���� ������
            _ef.GetCurrencyCount();
            switch (_settings.Technology)
            {
                case (int)Technology.AdoNet:
                    _activeRepo = _ado;
                    AdoRadio.Checked = true;
                    _technology = "ado";
                    break;
                case (int)Technology.EntityFramework:
                    _activeRepo = _ef;
                    EfRadio.Checked = true;
                    _technology = "ef";
                    break;
            }
            switch (_settings.RedisState)
            {
                case (int)RedisUsed.Off:
                    RedisLabel.Text = "��������";
                    _redisUsed = false;
                    break;
                case (int)RedisUsed.On:
                    RedisLabel.Text = "���������";
                    _redisUsed = true;
                    // ������ � Redis ��������� ������ ��� Entity Framework
                    _activeRepo = _ef;
                    EfRadio.Checked = true;
                    _technology = "ef";
                    EfRadio.Enabled = false;
                    AdoRadio.Enabled = false;
                    break;
            }
            _elastic = new Elastic();            
        }


        private void ExitButton_Click(object sender, EventArgs e)
        {
            _elastic.AddLogRecord("���������� ������", _technology, _redisUsed);
            this.Close();
            this.Dispose();
        }


        // ������ "��������� � ��: ������ �����"
        private void SaveCurListButton_Click(object sender, EventArgs e)
        {
            if (Val_code == null)
            {
                MessageBox.Show("�� ������� ������ �����!\n���� 'Connect to Server' ..", "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if(_activeRepo.GetCurrencyCount() == 0)
            {
                int nRow = _activeRepo.SaveCurrencyList(_cbrCurrencyList);
                string logText = $"� ������� '������ �����' ��������� {nRow} �������!";
                SaveCurListButton.Enabled = false;
                _elastic.AddLogRecord(logText, _technology, _redisUsed);
                MessageBox.Show(logText, "������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        // ������ "��������� � ��: ���� ������"
        private void SaveDateCursButton_Click(object sender, EventArgs e)
        {
            if (Val_code == null)
            {
                MessageBox.Show("�� ������� ������ �����!\n���� 'Connect to Server' ..", "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (_cbrCursDinamic == null || _cbrCursDinamic.Count == 0)
            {
                MessageBox.Show("�� ������� ������ ������ ������!\n���� '�������� ����' ..", "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (_cbrCursDinamic.Count > 0)
            {
                int addRecords = _activeRepo.AddCursDinamic(_cbrCursDinamic);
                string logText = $"� ������� 'CursDinamics' ��������� {addRecords} �������";
                _elastic.AddLogRecord(logText, _technology, _redisUsed);
                _cbrCursDinamic.Clear();
                if (_activeRepo.GetCursDinamicCount() > 5)
                    TestRunButton.Enabled = true;
                MessageBox.Show(logText, "������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        // ������ "����������" - ��������� ����� � ����������� ��������� �������
        private void OpenTableButton_Click(object sender, EventArgs e)
        {
            CurrListForm form = new CurrListForm(_activeRepo, TablesComboBox.SelectedIndex)
            {
                Text = TablesComboBox.SelectedItem.ToString()
            };
            form.Show();
            _elastic.AddLogRecord($"�������� ������� ������� '{TablesComboBox.SelectedItem}'", _technology, _redisUsed);
        }


        // ������ "������ �����"
        private void ChartFormButton_Click(object sender, EventArgs e)
        {
            if (Val_code == null)
            {
                MessageBox.Show("�� ������� ������!\n���� 'Connect to Server' ..", "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            var cursDinamic = _activeRepo.GetCursDinamic(Val_code);
            if (cursDinamic.Count == 0)
            {
                MessageBox.Show("��� ���� ������ ��� ������� � �������!\n���� �������� ���� � ��������� � �� ..", "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string currencyName = _activeRepo.GetCurrencyName(Val_code);
            CursChartForm form = new CursChartForm(cursDinamic, currencyName);
            _elastic.AddLogRecord($"�������� ������ ����� ������ {Val_code} ({currencyName})", _technology, _redisUsed);
            form.Show();
        }


        // ��������� ������� "������ ������ ������ � �� - Entity Framework"
        private void EfRadio_Click(object sender, EventArgs e)
        {
            _technology = "ef";
            _elastic.AddLogRecord("������ ������ ������ � �� -> Entity Framework", _technology, _redisUsed);
            _activeRepo = _ef;
        }


        // ��������� ������� "������ ������ ������ � �� - ADO.NET"
        private void AdoRadio_Click(object sender, EventArgs e)
        {
            _technology = "ado";
            _elastic.AddLogRecord("������ ������ ������ � �� -> ADO.NET", _technology, _redisUsed);
            _activeRepo = _ado;
        }


        // ��������� ������� "������� ������"
        private void listBox1_Click(object sender, EventArgs e)
        {
            if (Val_code != null && _activeRepo.GetCurrencyCount() > 0)
            {                
                string currencyName = _activeRepo.GetCurrencyName(Val_code);
                _elastic.AddLogRecord($"������� ������ {Val_code} ({currencyName})", _technology, _redisUsed);
            }
        }


        // ������ "�������� �������"
        private void ClearTablesButton_Click(object sender, EventArgs e)
        {
            string logText;
            DialogResult result = MessageBox.Show("������� ��� ������ �� ���� ������?", "",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2);
            if (result == DialogResult.No)
                return;
            if (_activeRepo.ClearAllTables())
            {
                logText = "��� ������ �� ������ �������!";
                _elastic.AddLogRecord(logText, _technology, _redisUsed);
                SaveCurListButton.Enabled = true;
                TestRunButton.Enabled = false;
                TestResultButton.Enabled = false;
                MessageBox.Show(logText, "������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (_elastic.ClearIndex())
            {
                logText = "���� �������!";
                _elastic.AddLogRecord(logText, _technology, _redisUsed);
                MessageBox.Show(logText, "������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        // ������ "��������� �����"
        private void TestResultButton_Click(object sender, EventArgs e)
        {
            List<Timing> timing = _activeRepo.GetTiming();
            if (timing.Count == 0)
            {
                MessageBox.Show("���� �� ����������!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            TestResultForm form = new TestResultForm(timing, _redisUsed);
            form.Show();
        }


        // ������ "����"
        private void TestRunButton_Click(object sender, EventArgs e)
        {
            if (_activeRepo.GetCursDinamicCount() <= 5)
            {
                MessageBox.Show("������������ ���������� � ������ �����!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            AdoRadio.Enabled = false;
            EfRadio.Enabled = false;
            TestRunButton.Enabled = false;

            // �������� ��������������� �������
            string logText = "������� ��������������� ������� ...";
            statusBar1.Text = logText;
            _elastic.AddLogRecord(logText, _technology, _redisUsed);
            _activeRepo.ClearTable("auxiliaries");

            // �������� ������� ������� ���������� �������� �� ������� � ������� ���������� Redis
            logText = $"������� ������� ������� ���������� ��������, ����������� ��� Redis = {_redisUsed} ...";
            statusBar1.Text = logText;
            _elastic.AddLogRecord(logText, _technology, _redisUsed);
            _activeRepo.CleanByCondition("timings", _redisUsed);
            // ������ id �� ������� curs_dinamics 
            List<int> cursId = _activeRepo.GetIdList();
            // ������� ��� ����� ������� SaveTiming � ����� MainForm
            _sender = new FormWriter(ChangeLabelText);

            if (_redisUsed)
            {
                TestRunWithRedis(cursId);
            }
            else
            {
                TestRunNoRedis(cursId);
            }

            MessageBox.Show("���� ��������!", "������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            statusBar1.Text = string.Empty;
            AdoRadio.Enabled = true;
            EfRadio.Enabled = true;
            TestRunButton.Enabled = true;
            TestResultButton.Enabled = true;
        }


        /// <summary>
        /// ������ ����� ��� Redis.
        /// </summary>
        /// <param name="cursId">������ id ������� curs_dinamics, ��� ������� ���������� ������������</param>
        private void TestRunNoRedis(List<int> cursId)
        { 
            // ��� ����� "������-������" ��������� �� ������� curs_dinamics � ������� auxiliaries � ������� ������� �� ������ ��������.
            // 1. � �������������� ���������� Entity Framework ��� ����������� � Redis
            _activeRepo = _ef;
            string logText = "���������� ����� ������ � ������ � �������������� Entity Framework ��� Redis ...";
            statusBar1.Text = logText;
            _technology = "ef";
            _elastic.AddLogRecord(logText, _technology, _redisUsed);
            EfRadio.Checked = true;
            // ���� ������
            DataCollection.SaveTiming(_activeRepo, cursId, _technology, _redisUsed, _sender);

            // 2. � �������������� ���������� ADO.NET ��� ����������� � Redis
            _activeRepo = _ado;
            logText = "���������� ����� ������ � ������ � �������������� ADO.NET ��� Redis ...";
            statusBar1.Text = logText;
            _technology = "ado";
            _elastic.AddLogRecord(logText, _technology, _redisUsed);
            AdoRadio.Checked = true;
            // ���� ������
            DataCollection.SaveTiming(_activeRepo, cursId, _technology, _redisUsed, _sender);
        }


        /// <summary>
        /// ������ ����� c Redis.
        /// </summary>
        /// <param name="cursId">������ id ������� curs_dinamics, ��� ������� ���������� ������������</param>
        private void TestRunWithRedis(List<int> cursId)
        {
            // � �������������� ���������� Entity Framework � ������������ � Redis
            string logText = "���������� ����� ������ � ������ � �������������� Entity Framework � Redis ...";
            statusBar1.Text = logText;
            _elastic.AddLogRecord(logText, _technology, _redisUsed);
            // ���� ������
            DataCollection.SaveTiming(_activeRepo, cursId, _technology, _redisUsed, _sender);
        }


        // ����������� ������, ���������� �� ������� DataCollection, � �������� ������������ ������� �� ������� �����
        private void ChangeLabelText(int n, int count)
        {
            StrNumLabel.Text = $"{n}/{count}";
        }

        #endregion

    }


    public class Valute
    {
        private string str_ValuteCode;
        private string str_ValuteName;

        public Valute(string ValuteNameStr, string ValuteCodeStr)
        {

            this.str_ValuteCode = ValuteCodeStr;
            this.str_ValuteName = ValuteNameStr;
        }

        public string ValuteCode
        {
            get
            {
                return str_ValuteCode;
            }
        }

        public string ValuteName
        {

            get
            {
                return str_ValuteName;
            }
        }

        public override string ToString()
        {
            return this.str_ValuteName + " - " + this.str_ValuteCode;
        }
       
    }

}
