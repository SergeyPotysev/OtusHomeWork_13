﻿using CurrencyQuery.Repository;
using System;
using System.Data;
using System.Windows.Forms;

namespace CurrencyQuery
{
    public partial class CurrListForm : Form
    {
        private UsingRepo _repo;
        private int _tableIndex;

        public CurrListForm(UsingRepo activeRepo, int tableIndex)
        {
            _repo = activeRepo;
            _tableIndex = tableIndex;     
            InitializeComponent();           
        }


        private void CurrListForm_Load(object sender, EventArgs e)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = _repo.GetCurrencyList(_tableIndex);
            bindingSource.DataSource = dataTable;
            CurrListDataGrid.DataSource = bindingSource;
            // Скрыть служебные поля Entity Framework
            if (_tableIndex < 3)
            {
                int n = CurrListDataGrid.Columns.Count;
                CurrListDataGrid.Columns[n - 1].Visible = false;
            }
            CurrListDataGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
        }


        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}