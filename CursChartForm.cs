﻿using CurrencyQuery.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CurrencyQuery
{
    public partial class CursChartForm : Form
    {
        public CursChartForm(List<CursDinamic> cursDinamic, string currencyName)
        {
            InitializeComponent();

            var chart = MyChart.ChartAreas[0];
            Axis ax = new Axis
            {
                Title = "Дата"
            };
            Axis ay = new Axis
            {
                Title = "Курс валюты"
            };
            chart.AxisX = ax;
            chart.AxisY = ay;
            chart.AxisX.IntervalType = DateTimeIntervalType.Days;
            chart.AxisX.LabelStyle.Format = "dd.MM";
            chart.AxisY.LabelStyle.Format = "0.00";
            chart.AxisY.LabelStyle.IsEndLabelVisible = true;
            chart.AxisX.Minimum = cursDinamic.Min(x => x.OnDate).ToOADate();
            chart.AxisX.Maximum = cursDinamic.Max(x => x.OnDate).ToOADate();
            // Будет 30 делений по оси X
            if (cursDinamic.Count <= 30)
                chart.AxisX.Interval = 1;
            else
                chart.AxisX.Interval = cursDinamic.Count / 30;
            chart.AxisY.Minimum = (double)cursDinamic.Min(y => y.Curs);
            chart.AxisY.Maximum = (double)cursDinamic.Max(y => y.Curs);
            // Будет 30 делений по оси Y
            chart.AxisY.Interval = (chart.AxisY.Maximum - chart.AxisY.Minimum) / 30;

            MyChart.Series.Add(currencyName);
            MyChart.Series[currencyName].ChartType = SeriesChartType.Spline;
            MyChart.Series[currencyName].XValueType = ChartValueType.DateTime;
            MyChart.Series[currencyName].Color = Color.Red;
            MyChart.Series[currencyName].BorderWidth = 2;
            MyChart.Series[currencyName].IsVisibleInLegend = true;

            foreach (CursDinamic item in cursDinamic)
            {
                MyChart.Series[currencyName].Points.AddXY(item.OnDate, item.Curs);
            }
        }


        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}