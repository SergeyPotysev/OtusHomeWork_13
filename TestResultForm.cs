﻿using CurrencyQuery.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CurrencyQuery
{
    public partial class TestResultForm : Form
    {
        public TestResultForm(List<Timing> timing, bool redisUsed)
        {
            InitializeComponent();

            var chart = TestResultChart.ChartAreas[0];
            Axis ax = new Axis
            {
                Title = "Итерации"
            };
            Axis ay = new Axis
            {
                Title = "Время выполнения (ticks)"
            };                       
            chart.AxisX = ax;
            chart.AxisY = ay;
            chart.AxisY.LabelStyle.Format = "0.0";
            chart.AxisY.LabelStyle.IsEndLabelVisible = true;
            chart.AxisX.LabelStyle.Angle = -90;

            // Проводился ли тест для Entity Framework с включенным Redis?
            bool redisOn = timing.Any(x => x.RedisUsed == true);
            // Проводился ли тест для Entity Framework и ADO.NET без Redis? 
            bool redisOff = timing.Any(x => x.RedisUsed == false);

            // Есть записи с теста с использованием Redis
            if (redisOn)
            {
                // Entity Framework с Redis
                // Т.к. ~ 5 первых значений больше остального ряда примерно в 100 раз их пропускаем
                List<Timing> efWithRedis = timing.Where(y => (y.Technology == "ef" && y.RedisUsed)).Skip(5).ToList();
                chart.AxisX.Minimum = efWithRedis.Min(x => x.CursDinamicId);
                chart.AxisX.Maximum = efWithRedis.Max(x => x.CursDinamicId);
                // Будет 50 делений по оси X
                if (efWithRedis.Count <= 50)
                    chart.AxisX.Interval = 1;
                else
                    chart.AxisX.Interval = efWithRedis.Count / 50;

                // Минимальное значение времени чтения Entity Framework с Redis
                var min1 = efWithRedis.Where(y => (y.Technology == "ef" && y.RedisUsed)).Min(y => y.ReadTime);
                // Минимальное значение времени записи Entity Framework c Redis
                var min2 = efWithRedis.Where(y => (y.Technology == "ef" && y.RedisUsed)).Min(y => y.WriteTime);
                // Максимальное значение времени чтения Entity Framework с Redis
                var max1 = efWithRedis.Where(y => (y.Technology == "ef" && y.RedisUsed)).Max(y => y.ReadTime);
                // Максимальное значение времени записи Entity Framework с Redis
                var max2 = efWithRedis.Where(y => (y.Technology == "ef" && y.RedisUsed)).Max(y => y.WriteTime);
                chart.AxisY.Minimum = (min1 < min2) ? min1 : min2;
                chart.AxisY.Maximum = (max1 > max2) ? max1 : max2;

                // Будет 50 делений по оси Y
                chart.AxisY.Interval = (chart.AxisY.Maximum - chart.AxisY.Minimum) / 50;

                string chartName = "EF+Redis Чтение";
                TestResultChart.Series.Add(chartName);
                TestResultChart.Series[chartName].ChartType = SeriesChartType.Spline;
                TestResultChart.Series[chartName].XValueType = ChartValueType.Int32;
                TestResultChart.Series[chartName].Color = Color.DarkMagenta;
                TestResultChart.Series[chartName].BorderWidth = 2;
                TestResultChart.Series[chartName].IsVisibleInLegend = true;
                foreach (Timing item in efWithRedis)
                {
                    TestResultChart.Series[chartName].Points.AddXY(item.CursDinamicId, item.ReadTime);
                }

                chartName = "EF+Redis Запись";
                TestResultChart.Series.Add(chartName);
                TestResultChart.Series[chartName].ChartType = SeriesChartType.Spline;
                TestResultChart.Series[chartName].XValueType = ChartValueType.Int32;
                TestResultChart.Series[chartName].Color = Color.DarkOrange;
                TestResultChart.Series[chartName].BorderWidth = 2;
                TestResultChart.Series[chartName].IsVisibleInLegend = true;
                foreach (Timing item in efWithRedis)
                {
                    TestResultChart.Series[chartName].Points.AddXY(item.CursDinamicId, item.WriteTime);
                }
            }

            // Есть записи с теста без использования Redis
            if(redisOff)
            { 
                // Entity Framework без Redis
                List<Timing> efNoRedis = timing.Where(y => (y.Technology == "ef" && !y.RedisUsed)).Skip(5).ToList();
                // ADO.NET без Redis
                List<Timing> adoNoRedis = timing.Where(y => (y.Technology == "ado" && !y.RedisUsed)).Skip(5).ToList(); 
                chart.AxisX.Minimum = efNoRedis.Min(x => x.CursDinamicId);
                chart.AxisX.Maximum = efNoRedis.Max(x => x.CursDinamicId);
 
                // Будет 50 делений по оси X
                if (efNoRedis.Count <= 50)
                    chart.AxisX.Interval = 1;
                else
                    chart.AxisX.Interval = efNoRedis.Count / 50;
            
                // Минимальное значение времени чтения Entity Framework без Redis
                var min1 = efNoRedis.Where(y => (y.Technology == "ef" && !y.RedisUsed)).Min(y => y.ReadTime);
                // Минимальное значение времени записи Entity Framework без Redis
                var min2 = efNoRedis.Where(y => (y.Technology == "ef" && !y.RedisUsed)).Min(y => y.WriteTime);
                // Максимальное значение времени чтения Entity Framework без Redis
                var max1 = efNoRedis.Where(y => (y.Technology == "ef" && !y.RedisUsed)).Max(y => y.ReadTime);
                // Максимальное значение времени записи Entity Framework без Redis
                var max2 = efNoRedis.Where(y => (y.Technology == "ef" && !y.RedisUsed)).Max(y => y.WriteTime);
                var minEf = (min1 < min2) ? min1 : min2;
                var maxEf = (max1 > max2) ? max1 : max2;

                // Минимальное значение времени чтения ADO.NET без Redis
                min1 = adoNoRedis.Where(y => (y.Technology == "ado" && !y.RedisUsed)).Min(y => y.ReadTime);
                // Минимальное значение времени записи ADO.NET без Redis
                min2 = adoNoRedis.Where(y => (y.Technology == "ado" && !y.RedisUsed)).Min(y => y.WriteTime);
                // Максимальное значение времени чтения ADO.NET без Redis
                max1 = adoNoRedis.Where(y => (y.Technology == "ado" && !y.RedisUsed)).Max(y => y.ReadTime);
                // Максимальное значение времени записи ADO.NET без Redis
                max2 = adoNoRedis.Where(y => (y.Technology == "ado" && !y.RedisUsed)).Max(y => y.WriteTime);
                var minAdo = (min1 < min2) ? min1 : min2;
                var maxAdo = (max1 > max2) ? max1 : max2;

                chart.AxisY.Minimum = (minEf < minAdo) ? minEf : minAdo;
                chart.AxisY.Maximum = (maxEf > maxAdo) ? maxEf : maxAdo;

                // Будет 50 делений по оси Y
                chart.AxisY.Interval = (chart.AxisY.Maximum - chart.AxisY.Minimum) / 50;

                string chartName = "EF Чтение";
                TestResultChart.Series.Add(chartName);
                TestResultChart.Series[chartName].ChartType = SeriesChartType.Spline;
                TestResultChart.Series[chartName].XValueType = ChartValueType.Int32;
                TestResultChart.Series[chartName].Color = Color.Blue;
                TestResultChart.Series[chartName].BorderWidth = 2;
                TestResultChart.Series[chartName].IsVisibleInLegend = true;
                foreach (Timing item in efNoRedis)
                {
                    TestResultChart.Series[chartName].Points.AddXY(item.CursDinamicId, item.ReadTime);
                }

                chartName = "EF Запись";
                TestResultChart.Series.Add(chartName);
                TestResultChart.Series[chartName].ChartType = SeriesChartType.Spline;
                TestResultChart.Series[chartName].XValueType = ChartValueType.Int32;
                TestResultChart.Series[chartName].Color = Color.DarkTurquoise; 
                TestResultChart.Series[chartName].BorderWidth = 2;
                TestResultChart.Series[chartName].IsVisibleInLegend = true;
                foreach (Timing item in efNoRedis)
                {
                    TestResultChart.Series[chartName].Points.AddXY(item.CursDinamicId, item.WriteTime);
                }

                chartName = "ADO Чтение";
                TestResultChart.Series.Add(chartName);
                TestResultChart.Series[chartName].ChartType = SeriesChartType.Spline;
                TestResultChart.Series[chartName].XValueType = ChartValueType.Int32;
                TestResultChart.Series[chartName].Color = Color.Red;
                TestResultChart.Series[chartName].BorderWidth = 2;
                TestResultChart.Series[chartName].IsVisibleInLegend = true;
                foreach (Timing item in adoNoRedis)
                {
                    TestResultChart.Series[chartName].Points.AddXY(item.CursDinamicId, item.ReadTime);
                }

                chartName = "ADO Запись";
                TestResultChart.Series.Add(chartName);
                TestResultChart.Series[chartName].ChartType = SeriesChartType.Spline;
                TestResultChart.Series[chartName].XValueType = ChartValueType.Int32;
                TestResultChart.Series[chartName].Color = Color.Green;
                TestResultChart.Series[chartName].BorderWidth = 2;
                TestResultChart.Series[chartName].IsVisibleInLegend = true;
                foreach (Timing item in adoNoRedis)
                {
                    TestResultChart.Series[chartName].Points.AddXY(item.CursDinamicId, item.WriteTime);
                }
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void TestResultForm_Load(object sender, EventArgs e)
        {

        }
    }
}