﻿using CurrencyQuery.Models;
using CurrencyQuery.Repository;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using static CurrencyQuery.MainForm;

namespace CurrencyQuery.Service
{
    public class DataCollection
    {
        public static void SaveTiming(UsingRepo repo, List<int> cursId, string technology, bool redisUsed, FormWriter sender)
        {
            int n = 0;
            var sw = new Stopwatch();
            long writeResult, readResult;
            foreach (int id in cursId)
            {
                CursDinamic cd = new CursDinamic();
                sw.Start();
                // Чтение 
                cd = repo.GetRecord(id);
                // Измерение времени чтения
                readResult = sw.ElapsedTicks;
                sw.Reset();
                Auxiliary aux = new Auxiliary
                {
                    CursId = cd.Id,
                    OnDate = cd.OnDate,
                    CurrencyId = cd.CurrencyId,
                    Curs = cd.Curs,
                    Technology = technology,
                    RedisUsed = redisUsed
                };
                sw.Start();
                // Сохранение
                repo.AddRecord(aux);
                // Измерение времени сохранения
                writeResult = sw.ElapsedTicks;
                sw.Reset();
                // Сохранение времени выполнения операций чтения и записи
                repo.AddTiming(new Timing
                {
                    Technology = technology,
                    RedisUsed = redisUsed,
                    CursDinamicId = id,
                    ReadTime = readResult,
                    WriteTime = writeResult
                });
                ++n;
                // Отправить данные счетчика обработанных записей на главную форму
                sender(n, cursId.Count);
                Application.DoEvents();
            }
        }
    }
}
