﻿using System;

namespace CurrencyQuery.Models
{
    public class LogRecord
    {
        public DateTime LogTime { get; set; }
        public string LogText { get; set; }
        public string Technology { get; set; }
        public bool RedisUsed { get; set; } 
    }
}
