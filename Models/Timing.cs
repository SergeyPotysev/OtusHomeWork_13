﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyQuery.Models
{
    [Table("timings")]
    public class Timing
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        ///Способ  работы с БД.
        /// </summary>
        [Column("technology")]
        public string Technology { get; set; }

        /// <summary>
        ///Использование Redis.
        /// </summary>
        [Column("redis_used")]
        public bool RedisUsed { get; set; }

        /// <summary>
        ///Идентификатор операции получения курса валюты.
        /// </summary>
        [Column("curs_dinamic_id")]
        public int CursDinamicId { get; set; }

        /// <summary>
        ///Длительность операции получения данных из БД.
        /// </summary>
        [Column("read_time")]
        public long ReadTime { get; set; }

        /// <summary>
        ///Длительность операции сохранения данных в БД.
        /// </summary>
        [Column("write_time")]
        public long WriteTime { get; set; }

        public CursDinamic CursDinamic { get; set; }
    }
}
