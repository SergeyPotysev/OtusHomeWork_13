﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyQuery.Models
{
    [Table("curs_dinamics")]
    public class CursDinamic
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        /// <summary>
        /// Дата запроса курса валюты.
        /// </summary>
        [Column("on_date")]
        public DateTime OnDate { get; set; }

        /// <summary>
        /// Вид валюты.
        /// </summary>
        [Column("currency_id")]
        public int CurrencyId { get; set; }

        /// <summary>
        /// Курс.
        /// </summary>
        [Column("curs")]
        public decimal Curs { get; set; }

        public Currency Currency { get; set; }
    }
}