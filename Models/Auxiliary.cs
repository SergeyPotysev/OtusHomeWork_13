﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyQuery.Models
{
    [Table("auxiliaries")]
    public class Auxiliary
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Id из таблицы CursDinamic
        /// </summary>
        [Column("curs_id")]
        public int CursId { get; set; }

        /// <summary>
        /// Дата запроса курса валюты.
        /// </summary>
        [Column("on_date")]
        public DateTime OnDate { get; set; }

        /// <summary>
        /// Вид валюты.
        /// </summary>
        [Column("currency_id")]
        public int CurrencyId { get; set; }

        /// <summary>
        /// Курс.
        /// </summary>
        [Column("curs")]
        public decimal Curs { get; set; }

        /// <summary>
        /// Используемая технология работы с БД.
        /// </summary>
        [Column("technology")]
        public string Technology { get; set; }

        /// <summary>
        /// Использование кэширования в Redis.
        /// </summary>
        [Column("redis_used")]
        public bool RedisUsed { get; set; }
    }
}