﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyQuery.Models
{
    [Table("currencies")]
    public class Currency
    {
        // Id первичный ключ
        [Key]
        // В БД поле будет называться id
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Внутренний код валюты.
        /// </summary>
        [Column("code")]
        public string Code { get; set; }

        /// <summary>
        /// Название валюты на русском языке.
        /// </summary>
        [Column("rus_name")]
        public string RusName { get; set; }

        /// <summary>
        /// Название валюты на английском языке.
        /// </summary>
        [Column("eng_name")]
        public string EngName { get; set; }

        /// <summary>
        /// Номинал.
        /// </summary>
        [Column("nominal")]
        public decimal Nominal { get; set; }

        /// <summary>
        /// Внутренний код валюты, являющейся базовой.
        /// </summary>
        [Column("common_code")]
        public string CommonCode { get; set; }

        /// <summary>
        /// Цифровой код ISO.
        /// </summary>
        [Column("num_code")]
        public int NumCode { get; set; }

        /// <summary>
        /// Трехбуквенный код ISO.
        /// </summary>
        [Column("char_code")]
        public string CharCode { get; set; }

        public ICollection<CursDinamic> CursDinamics { get; set; }
    }
}