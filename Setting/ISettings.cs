﻿namespace CurrencyQuery.Setting
{
    public interface ISettings
    {
        int Technology { get; }
        int RedisState { get; }
    }
}