﻿using Microsoft.Extensions.Configuration;
using System.Configuration;
using System.IO;

namespace CurrencyQuery.Setting
{
    public enum Technology
    {
        AdoNet = 0,
        EntityFramework = 1
    }

    public enum RedisUsed
    {
        Off = 0,
        On = 1        
    }


    public class Settings : ISettings
    {
        /// <summary>
        /// Способ работы с БД: ADO.NET или EF.
        /// </summary>
        public int Technology { get; private set; }

        /// <summary>
        /// Состояние Redis: отключен/подключен.
        /// </summary>
        public int RedisState { get; private set; }

        public Settings(string basePath, string file)
        {
            var builder = new Microsoft.Extensions.Configuration.ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile(file);
            IConfiguration config = builder.Build();
            Technology = int.Parse(config[nameof(Technology)]);
            RedisState = int.Parse(config[nameof(RedisState)]);
        }


        /// <summary>
        /// Строка подключения к БД.
        /// </summary>
        public static string GetConnStr(string name)
        {
            // Assume failure.
            string connStr = null;
            // Look for the name in the connectionStrings section.
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];
            // If found, return the connection string.
            if (settings != null)
                connStr = settings.ConnectionString;
            return connStr;
        }


        public static Settings GetAppJson()
        {
            string appPath = Directory.GetCurrentDirectory();
            return new Settings(appPath, "appsettings.json");
        }
    }
}