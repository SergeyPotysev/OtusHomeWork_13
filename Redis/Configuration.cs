﻿using EFCache;
using EFCache.Redis;
using CurrencyQuery.Setting;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Common;

namespace CurrencyQuery.Redis
{
    public class Configuration : DbConfiguration
    {
        private Settings _settings;
        public Configuration()
        {
            _settings = Settings.GetAppJson();
            // Подключить Redis, если в appsettings.json указано - RedisState : 1
            if(_settings.RedisState == (int)RedisUsed.On)
            {
                var redisConnection = ConfigurationManager.ConnectionStrings["Redis"].ConnectionString;
                var cache = new RedisCache(redisConnection);
                var transactionHandler = new CacheTransactionHandler(cache);                
                AddInterceptor(transactionHandler);
                Loaded += (sender, args) =>
                {
                    args.ReplaceService<DbProviderServices>((s, _) => new CachingProviderServices(s, transactionHandler, new RedisCachingPolicy()));
                };
            }
        }
    }

}
