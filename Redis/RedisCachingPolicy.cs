﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Metadata.Edm;
using EFCache;

namespace CurrencyQuery.Redis
{
    public class RedisCachingPolicy : CachingPolicy
    {
        protected override void GetExpirationTimeout(
            ReadOnlyCollection<EntitySetBase> affectedEntitySets, 
            out TimeSpan slidingExpiration, 
            out DateTimeOffset absoluteExpiration)
        {
            // Срок действия записи в кеше может истечь, если к ней не обращались в течение 5 минут
            slidingExpiration = TimeSpan.FromMinutes(5);
            // Каждая запись в кеше истекает через 30 минут
            absoluteExpiration = DateTimeOffset.Now.AddMinutes(30);
        }
    }
}
