﻿using CurrencyQuery.Models;
using Nest;
using CurrencyQuery.Repository;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows.Forms;


namespace CurrencyQuery.Elasticsearch
{
    class Elastic
    {
        //https://www.elastic.co/guide/en/elasticsearch/client/net-api/current/nest-getting-started.html
        private readonly Nest.ConnectionSettings _settings;
        private readonly Nest.ElasticClient _client;

        public Elastic()
        {
            //Индекс при подключении будет создан, если его - нет, использован, если существует 
            var elasticConnection = ConfigurationManager.ConnectionStrings["Elastic"].ConnectionString;
            _settings = new ConnectionSettings(new UriBuilder(elasticConnection).Uri).DefaultIndex("monitor");
            _client = new ElasticClient(_settings);
        }


        /// <summary>
        /// Добавление объекта лог-записи в индекс.
        /// </summary>
        /// <param name="log">Объект лог-записи</param>
        public void IndexLog(LogRecord log)
        {
            var response = _client.IndexDocument(log);
        }


        /// <summary>
        /// Добавление объекта лог-записи в индекс. Асинхронный способ. 
        /// </summary>
        /// <param name="log">Объект лог-записи</param>
        private async void IndexLogAsync(LogRecord log)
        {
            var asyncIndexResponse = await _client.IndexDocumentAsync(log);
            if(!asyncIndexResponse.ApiCall.Success)
            {
                MessageBox.Show("Ошибка записи в Elasticserch!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// Получение всех документов индекса с типом LogRecord.
        /// </summary>
        /// <returns>Объект DataTable для размещения в DataGridView.</returns>
         public DataTable GetLogs()
         {
            int records = Convert.ToInt32(_client.Count<LogRecord>().Count);
            var searchResponse = _client.Search<LogRecord>(s => s.From(0).Size(records).MatchAll().Query(q => q));
            return UsingRepo.ToDataTable(searchResponse.Documents.ToList());
         }


        /// <summary>
        /// Удаление индекса.
        /// </summary>
        /// <returns>Результат удаления.</returns>
        public bool DropIndex()
        {
            return _client.Indices.Delete("monitor").IsValid;           
        }


        /// <summary>
        /// Удаление из индекса всех документов с типом LogRecord.
        /// </summary>
        /// <returns>Результат удаления.</returns>
        public bool ClearIndex()
        {            
            return _client.DeleteByQuery<LogRecord>(del => del.Query(q => q.QueryString(qs => qs.Query("*")))).IsValid;
        }


        /// <summary>
        /// Создание лог-записи и добавление в индекс. 
        /// </summary>
        /// <param name="logText">Текст лог-сообщения</param>
        public void AddLogRecord(string logText, string technology, bool redisUsed)
        {
            LogRecord log = new LogRecord
            {
                LogTime = DateTime.Now,
                LogText = logText,
                Technology = technology,
                RedisUsed = redisUsed              
            };
            IndexLogAsync(log);
        }

    }

}
