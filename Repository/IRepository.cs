﻿using System.Collections.Generic;
using System.Data;
using CurrencyQuery.Models;

namespace CurrencyQuery.Repository
{
    public interface IRepository
    {
        /// <summary>
        /// Сохраняет список валют, полученных с сервера.
        /// </summary>
        /// <returns>Количество сохраненных записей.</returns>
        int SaveCurrencyList(List<Currency> currencies);

        /// <summary>
        /// Получение идентификатора курса валюты.
        /// </summary>
        /// <param name="currencyCode">Символьный код валюты.</param>
        /// <returns>Идетификатор курса валюты.</returns>
        int GetCurrencyId(string currencyCode);

        /// <summary>
        /// Запись новых данных о курсе валюты с проверкой на дублирование.
        /// </summary>
        /// <param name="cursDinamic">Список курсов валюты для записи в таблицу.</param>
        /// <returns>Количество добавленных записей.</returns>
        int AddCursDinamic(List<CursDinamic> cursDinamic);

        /// <summary>
        /// Возвращает все записи выбранной таблицы.
        /// </summary>
        /// <param name="tableIndex">Номер таблицы.</param>
        /// <returns>Содержимое таблицы в формате DataTable.</returns>
        DataTable GetCurrencyList(int tableIndex);

        /// <summary>
        /// Возвращает список курсов выбранной валюты.
        /// </summary>
        /// <param name="currencyCode">Символьный код валюты.</param>
        List<CursDinamic> GetCursDinamic(string currencyCode);

        /// <summary>
        /// Возвращает количество записей курсов валют.
        /// </summary>
        int GetCursDinamicCount();

        /// <summary>
        /// Возвращает название валюты на русском языке.
        /// </summary>
        /// <param name="currencyCode">Символьный код валюты.</param>
        string GetCurrencyName(string currencyCode);

        /// <summary>
        /// Удаление всех записей из всех таблиц кроме логов.
        /// </summary>
        /// <returns>Результат удаления.</returns>
        bool ClearAllTables();

        /// <summary>
        /// Очистить таблицу по имени. 
        /// </summary>
        /// <returns>Результат операции.</returns>
        bool ClearTable(string tableName);

        /// <summary>
        /// Очистить таблицу по имени с условием. 
        /// </summary>
        /// <returns>Результат операции.</returns>
        bool CleanByCondition(string tableName, bool redisUsed);

        /// <summary>
        /// Удаление всех таблиц.
        /// </summary>
        /// <returns>Результат удаления.</returns>
        bool DropTables();

        /// <summary>
        /// Возвращает количество сохраненных в БД валют.
        /// </summary>
        int GetCurrencyCount();

        /// <summary>
        /// Возвращает список id таблицы curs_dinamics
        /// </summary>
        List<int> GetIdList();

        /// <summary>
        /// Возвращает запись таблицы curs_dinamics по id.
        /// </summary>
        /// <param name="id">Идентификатор записи.</param>
        CursDinamic GetRecord(int id);

        /// <summary>
        /// Сохраняет тестовую запись во вспомогательную таблицу auxiliaries.
        /// </summary>
        void AddRecord(Auxiliary record);

        /// <summary>
        /// Сохраняет длительность операций чтения-записи.
        /// </summary>
        void AddTiming(Timing record);

        /// <summary>
        /// Возвращает список записей времени выполнения операций чтения/записи.
        /// </summary>
        List<Timing> GetTiming();

        /// <summary>
        /// Возвращает количество записей времени выполнения операций чтения/записи.
        /// </summary>
        int GetTimingCount();
    }
}