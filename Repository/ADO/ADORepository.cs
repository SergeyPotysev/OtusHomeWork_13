﻿using Npgsql;
using CurrencyQuery.Setting;
using CurrencyQuery.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using CurrencyQuery.Elasticsearch;

namespace CurrencyQuery.Repository.ADO
{
    class ADORepository : IRepository
    {
        private readonly string _connectionString;
        public ADORepository()
        {
            _connectionString = Settings.GetConnStr("Postgres");
        }


        public int SaveCurrencyList(List<Currency> сurrencies)
        {
            int nRow = 0;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "INSERT INTO currencies (code, rus_name, eng_name, nominal, common_code, num_code, char_code) ";
                    strSQL += "VALUES (@a1, @a2, @a3, @a4, @a5, @a6, @a7);";
                    pgConn.Open();
                    foreach (Currency curr in сurrencies)
                    {
                        using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                        {
                            pgCommand.Parameters.AddWithValue("a1", curr.Code);
                            pgCommand.Parameters.AddWithValue("a2", curr.RusName);
                            pgCommand.Parameters.AddWithValue("a3", curr.EngName);
                            pgCommand.Parameters.AddWithValue("a4", curr.Nominal);
                            pgCommand.Parameters.AddWithValue("a5", curr.CommonCode);
                            pgCommand.Parameters.AddWithValue("a6", curr.NumCode);
                            pgCommand.Parameters.AddWithValue("a7", curr.CharCode ?? string.Empty);
                            pgCommand.Prepare();
                            nRow += pgCommand.ExecuteNonQuery();
                        }
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return nRow;
        }


        public int GetCurrencyId(string currencyCode)
        {
            int id = -1;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "SELECT id FROM currencies WHERE code = @a1;";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", currencyCode);
                        pgCommand.Prepare();
                        id = Convert.ToInt32(pgCommand.ExecuteScalar());
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return id;
        }


        public int AddCursDinamic(List<CursDinamic> cursDinamic)
        {
            int nRow = 0;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "INSERT INTO curs_dinamics (on_date, currency_id, curs) SELECT @a1, @a2, @a3 WHERE NOT EXISTS " +
                                    "(SELECT 1 FROM curs_dinamics WHERE on_date = @a1 AND currency_id = @a2);";
                    pgConn.Open();
                    foreach (CursDinamic curs in cursDinamic)
                    {
                        using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                        {
                            pgCommand.Parameters.AddWithValue("a1", curs.OnDate);
                            pgCommand.Parameters.AddWithValue("a2", curs.CurrencyId);
                            pgCommand.Parameters.AddWithValue("a3", curs.Curs);
                            pgCommand.Prepare();
                            nRow += pgCommand.ExecuteNonQuery();
                        }
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return nRow;
        }


        private int RecordExists(CursDinamic curs)
        {
            int recordCount = 0;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "SELECT COUNT(*) FROM curs_dinamics WHERE on_date = @a1 AND currency_id = @a2;";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", curs.OnDate);
                        pgCommand.Parameters.AddWithValue("a2", curs.CurrencyId);
                        pgCommand.Prepare();
                        recordCount = Convert.ToInt32(pgCommand.ExecuteScalar());
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return recordCount;
        }


        public DataTable GetCurrencyList(int tableIndex)
        {
            DataTable dataTable = default;
            // Логи
            if (tableIndex == 4)
            {                 
                Elastic es = new Elastic();
                dataTable = es.GetLogs();
            }
            else
            {
                string[] tableName = new string[] { "currencies", "curs_dinamics", "timings", "auxiliaries" };
                using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
                {
                    try
                    {
                        string strSQL = "SELECT * FROM ";
                        pgConn.Open();
                        using (var pgCommand = new NpgsqlCommand(strSQL + tableName[tableIndex] + ";", pgConn))
                        {
                            pgCommand.Prepare();
                            var pgReader = pgCommand.ExecuteReader();
                            switch (tableIndex)
                            {
                                // Список валют
                                case 0:  
                                    List<Currency> currList = new List<Currency>();
                                    while (pgReader.Read())
                                    {
                                        currList.Add(new Currency
                                        {
                                            Id = Convert.ToInt32(pgReader[0]),
                                            Code = Convert.ToString(pgReader[1]),
                                            RusName = Convert.ToString(pgReader[2]),
                                            EngName = Convert.ToString(pgReader[3]),
                                            Nominal = Convert.ToDecimal(pgReader[4]),
                                            CommonCode = Convert.ToString(pgReader[5]),
                                            NumCode = (pgReader[6] != DBNull.Value) ? Convert.ToInt32(pgReader[6]) : default,
                                            CharCode = (pgReader[7] != DBNull.Value) ? Convert.ToString(pgReader[7]).Trim() : default
                                        });
                                    }
                                    dataTable = UsingRepo.ToDataTable<Currency>(currList);
                                    break;
                                // Динамика курса валюты
                                case 1:  
                                    List<CursDinamic> cursList = new List<CursDinamic>();
                                    while (pgReader.Read())
                                    {
                                        cursList.Add(new CursDinamic
                                        {
                                            Id = Convert.ToInt32(pgReader[0]),
                                            OnDate = Convert.ToDateTime(pgReader[1]),
                                            CurrencyId = Convert.ToInt32(pgReader[2]),
                                            Curs = Convert.ToDecimal(pgReader[3])
                                        });
                                    }
                                    dataTable = UsingRepo.ToDataTable<CursDinamic>(cursList);
                                    break;
                                // Тайминг
                                case 2:  
                                    List<Timing> timing = new List<Timing>();
                                    while (pgReader.Read())
                                    {
                                        timing.Add(new Timing
                                        {
                                            Id = Convert.ToInt32(pgReader[0]),
                                            Technology = Convert.ToString(pgReader[1]),
                                            RedisUsed = Convert.ToBoolean(pgReader[2]),
                                            CursDinamicId = Convert.ToInt32(pgReader[3]),
                                            ReadTime = (long)pgReader[4],
                                            WriteTime = (long)pgReader[5]
                                        });
                                    }
                                    dataTable = UsingRepo.ToDataTable<Timing>(timing);
                                    break;
                                // Тесты
                                case 3: 
                                    List<Auxiliary> auxiliary = new List<Auxiliary>();
                                    while (pgReader.Read())
                                    {
                                        auxiliary.Add(new Auxiliary
                                        {
                                            Id = Convert.ToInt32(pgReader[0]),
                                            CursId = Convert.ToInt32(pgReader[1]),
                                            OnDate = Convert.ToDateTime(pgReader[2]),
                                            CurrencyId = Convert.ToInt32(pgReader[3]),
                                            Curs = Convert.ToDecimal(pgReader[4]),
                                            Technology = Convert.ToString(pgReader[5]),
                                            RedisUsed = Convert.ToBoolean(pgReader[6])
                                        });
                                    }
                                    dataTable = UsingRepo.ToDataTable<Auxiliary>(auxiliary);
                                    break;
                            }
                        }
                    }
                    catch (NpgsqlException ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            return dataTable;
        }


        public List<CursDinamic> GetCursDinamic(string currencyCode)
        {
            List<CursDinamic> CursDinamic = new List<CursDinamic>();
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    int id = GetCurrencyId(currencyCode);
                    pgConn.Open();
                    string strSQL = "SELECT * FROM curs_dinamics WHERE currency_id = @a1;";
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", id);
                        pgCommand.Prepare();
                        var pgReader = pgCommand.ExecuteReader();
                        while (pgReader.Read())
                        {
                            CursDinamic.Add(new CursDinamic
                            {
                                OnDate = Convert.ToDateTime(pgReader[1]),
                                CurrencyId = Convert.ToInt32(pgReader[2]),
                                Curs = Convert.ToDecimal(pgReader[3])
                            });
                        }
                    }               
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return CursDinamic;
        }


        public int GetCursDinamicCount()
        {
            return TableCount("curs_dinamics");
        }


        private int TableCount(string tableName)
        { 
            int recordCount = 0;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "SELECT COUNT(*) FROM " + tableName + ";";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Prepare();
                        recordCount = Convert.ToInt32(pgCommand.ExecuteScalar());
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return recordCount;
        }


        public string GetCurrencyName(string currencyCode)
        {
            string currencyName = string.Empty;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "SELECT rus_name FROM currencies WHERE code = @a1;";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", currencyCode);
                        pgCommand.Prepare();
                        currencyName = pgCommand.ExecuteScalar().ToString();
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return currencyName;
        }


        public bool DropTables()
        {
            return TableOperation("DROP TABLE IF EXISTS ");
        }


        public bool ClearAllTables()
        {
            bool result = true;
            bool next;
            string[] tableName = new string[] { "timings", "curs_dinamics", "currencies", "auxiliaries" };
            for (int i = 0; i < tableName.Count(); i++)
            {
                next = TableOperation("DELETE FROM " + tableName[i]);
                if(!next)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

 
        public bool ClearTable(string tableName)
        {
            return TableOperation("DELETE FROM " + tableName);
        }


        private bool TableOperation(string strSQL)
        {             
            bool result = true;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Prepare();
                        pgCommand.ExecuteNonQuery();
                    }
                }
                catch (NpgsqlException ex)
                {
                    result = false;
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return result;
        }


        public bool CleanByCondition(string tableName, bool redisUsed)
        {
            bool result = true;
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    pgConn.Open();
                    string strSQL = "DELETE FROM " + tableName + " WHERE redis_used = @a1;"; 
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", redisUsed);
                        pgCommand.Prepare();
                        pgCommand.ExecuteNonQuery();
                    }
                }
                catch (NpgsqlException ex)
                {
                    result = false;
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return result;
        }


        public int GetCurrencyCount()
        {
            return TableCount("currencies");
        }


        public List<int> GetIdList()
        {
            List<int> cursId = new List<int>();
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "SELECT id FROM curs_dinamics;";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Prepare();
                        var pgReader = pgCommand.ExecuteReader();
                        while (pgReader.Read())
                        {
                            cursId.Add(Convert.ToInt32(pgReader[0]));
                        }
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return cursId;
        }


        public CursDinamic GetRecord(int id)
        {
            CursDinamic cd = new CursDinamic();
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "SELECT * FROM curs_dinamics WHERE id = @a1;";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", id);
                        pgCommand.Prepare();
                        var pgReader = pgCommand.ExecuteReader();
                        if (pgReader.Read())
                        {
                            cd.Id = Convert.ToInt32(pgReader[0]);
                            cd.OnDate = Convert.ToDateTime(pgReader[1]);
                            cd.CurrencyId = Convert.ToInt32(pgReader[2]);
                            cd.Curs = Convert.ToDecimal(pgReader[3]);
                        }
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return cd;
        }


        public void AddRecord(Auxiliary record)
        {
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "INSERT INTO auxiliaries (curs_id, on_date, currency_id, curs, technology, redis_used) VALUES (@a1, @a2, @a3, @a4, @a5, @a6);";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", record.CursId);
                        pgCommand.Parameters.AddWithValue("a2", record.OnDate);
                        pgCommand.Parameters.AddWithValue("a3", record.CurrencyId);
                        pgCommand.Parameters.AddWithValue("a4", record.Curs);
                        pgCommand.Parameters.AddWithValue("a5", record.Technology);
                        pgCommand.Parameters.AddWithValue("a6", record.RedisUsed);
                        pgCommand.Prepare();
                        pgCommand.ExecuteNonQuery();
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        public void AddTiming(Timing record)
        {
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "INSERT INTO timings (technology, redis_used, curs_dinamic_id, read_time, write_time) VALUES (@a1, @a2, @a3, @a4, @a5);";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Parameters.AddWithValue("a1", record.Technology);
                        pgCommand.Parameters.AddWithValue("a2", record.RedisUsed);
                        pgCommand.Parameters.AddWithValue("a3", record.CursDinamicId);
                        pgCommand.Parameters.AddWithValue("a4", record.ReadTime);
                        pgCommand.Parameters.AddWithValue("a5", record.WriteTime);
                        pgCommand.Prepare();
                        pgCommand.ExecuteNonQuery();
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        public List<Timing> GetTiming()
        {
            List<Timing> timing = new List<Timing>();
            using (NpgsqlConnection pgConn = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    string strSQL = "SELECT * FROM timings;";
                    pgConn.Open();
                    using (var pgCommand = new NpgsqlCommand(strSQL, pgConn))
                    {
                        pgCommand.Prepare();
                        var pgReader = pgCommand.ExecuteReader();
                        while (pgReader.Read())
                        {
                            timing.Add(new Timing
                            {
                                Id = Convert.ToInt32(pgReader[0]),
                                Technology = pgReader[1].ToString(),
                                RedisUsed = Convert.ToBoolean(pgReader[2]),
                                CursDinamicId = Convert.ToInt32(pgReader[3]),
                                ReadTime = (long)pgReader[4],
                                WriteTime = (long)pgReader[5]
                            });
                        }  
                    }
                }
                catch (NpgsqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return timing;
        }


        public int GetTimingCount()
        {
            return TableCount("timings");
        }

    }
}