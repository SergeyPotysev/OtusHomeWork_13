﻿using CurrencyQuery.Elasticsearch;
using CurrencyQuery.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace CurrencyQuery.Repository.EF
{
    public class EFRepository : IRepository, IDisposable
    {
        private bool _disposed = false;
        private readonly ApplicationContext _context;
 
        public EFRepository()
        {
            _context = new ApplicationContext();
        }


        public int SaveCurrencyList(List<Currency> currencies)
        {
             int newRecords = _context.Currencies.AddRange(currencies).Count();
            _context.SaveChanges();
            return newRecords;
        }


        public int GetCurrencyId(string currencyCode)
        {
            return _context.Currencies.Single(x => x.Code.Equals(currencyCode)).Id;
        }


        public int AddCursDinamic(List<CursDinamic> cursDinamic)
        {
            int newRecords = 0;
            foreach(CursDinamic curs in cursDinamic)
            {
                // Проверка есть ли уже курс на такую же дату такой же валюты
                if (_context.CursDinamics.Any(x => (x.OnDate == curs.OnDate) && (x.CurrencyId == curs.CurrencyId))) 
                    continue;
                _context.CursDinamics.Add(curs);
                ++newRecords;
            }
            _context.SaveChanges();
            return newRecords;
        }


        public DataTable GetCurrencyList(int tableIndex)
        {
            DataTable dataTable = default;
            switch (tableIndex)
            {
                // Список валют
                case 0:  
                    List<Currency> currList = (from qq in _context.Currencies select qq).ToList();
                    dataTable = UsingRepo.ToDataTable<Currency>(currList);
                    break;
                // Динамика курса валюты
                case 1:  
                    List<CursDinamic> cursList = (from qq in _context.CursDinamics select qq).ToList();
                    dataTable = UsingRepo.ToDataTable<CursDinamic>(cursList);
                    break;
                // Тайминг
                case 2:  
                    List<Timing> timing = (from qq in _context.Timings select qq).ToList();
                    dataTable = UsingRepo.ToDataTable<Timing>(timing);
                    break;
                // Тесты
                case 3: 
                    List<Auxiliary> auxiliary = (from qq in _context.Auxiliaries select qq).ToList();
                    dataTable = UsingRepo.ToDataTable<Auxiliary>(auxiliary);
                    break;
                // Логи
                case 4:  
                    Elastic es = new Elastic();
                    dataTable = es.GetLogs();                    
                    break;
            }
            return dataTable;
        }


        public List<CursDinamic> GetCursDinamic(string currencyCode)
        {
            List<CursDinamic> cursDinamic = _context.CursDinamics.Where(x => x.Currency.Code.Equals(currencyCode)).ToList();
            return cursDinamic;
        }


        public int GetCursDinamicCount()
        {
            return _context.CursDinamics.Count();
        }


        public List<Timing> GetTiming()
        {
            return _context.Timings.ToList();
        }


        public int GetTimingCount()
        {
            return _context.Timings.Count();
        }


        public string GetCurrencyName(string currencyCode)
        {
            return _context.Currencies.Single(x => x.Code.Equals(currencyCode)).RusName;
        }


        public bool ClearAllTables()
        {
            bool result = true;
            try
            {
                _context.CursDinamics.RemoveRange(_context.CursDinamics);
                _context.Currencies.RemoveRange(_context.Currencies);
                _context.Timings.RemoveRange(_context.Timings);
                _context.Auxiliaries.RemoveRange(_context.Auxiliaries);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            return result;
        }


        public bool ClearTable(string tableName)
        {
            bool result = true;
            try
            {
                if (tableName == "auxiliaries")
                {
                    _context.Auxiliaries.RemoveRange(_context.Auxiliaries);
                    _context.SaveChanges();
                }
                if (tableName == "timings")
                {
                    _context.Timings.RemoveRange(_context.Timings);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            return result;
        }


        public bool CleanByCondition(string tableName, bool redisUsed)
        {
            bool result = true;
            try
            {
                if (tableName == "timings")
                {
                    _context.Timings.RemoveRange(_context.Timings.Where(x => x.RedisUsed == redisUsed));
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            return result;
        }


        public bool DropTables()
        {
            bool result = true;
            string[] tableName = new string[] { "curs_dinamics", "currencies", "timings", "auxiliaries" };
            string strSQL = string.Empty;
            try
            {
                foreach (string table in tableName)
                {
                    strSQL = "DROP TABLE " + table + ";";                                   
                }
                int dropResult = _context.Database.ExecuteSqlCommand(strSQL);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            return result;
        }

 
        public int GetCurrencyCount()
        {
           return _context.Currencies.Count();
        }


        public List<int> GetIdList()
        {
            return _context.CursDinamics.Select(x => x.Id).ToList();
        }


        public CursDinamic GetRecord(int id)
        {
            return _context.CursDinamics.Single(x => x.Id == id);
        }

 
        public void AddRecord(Auxiliary record)
        {
            _context.Auxiliaries.Add(record);
            _context.SaveChanges();
        }


        public void AddTiming(Timing record)
        {
            _context.Timings.Add(record);
            _context.SaveChanges();
        }


        /// <summary>
        /// Dispose of unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">Is already disposed?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }


        ~EFRepository()
        {
            Dispose(false);
        }
    }
}