﻿using System.Data.Entity;
using CurrencyQuery.Models;

namespace CurrencyQuery.Repository.EF
{
    internal class ApplicationContext : DbContext
    {
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<CursDinamic> CursDinamics { get; set; }
        public DbSet<Timing> Timings { get; set; }
        public DbSet<Auxiliary> Auxiliaries { get; set; }


        public ApplicationContext() : base(nameOrConnectionString: "Postgres")  
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
        }
    }
}