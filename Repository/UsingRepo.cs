﻿using CurrencyQuery.Models;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace CurrencyQuery.Repository
{
    public class UsingRepo : IRepository
    {
        private readonly IRepository _repository;
        public UsingRepo(IRepository repository)
        {
            _repository = repository;
        }


        public int GetCurrencyId(string currencyCode)
        {
            return _repository.GetCurrencyId(currencyCode);
        }


        public DataTable GetCurrencyList(int tableIndex)
        {
            return _repository.GetCurrencyList(tableIndex);
        }


        public List<CursDinamic> GetCursDinamic(string currencyCode)
        {
            return _repository.GetCursDinamic(currencyCode);
        }


        public string GetCurrencyName(string currencyCode)
        {
            return _repository.GetCurrencyName(currencyCode);
        }


        public int AddCursDinamic(List<CursDinamic> cursDinamic)
        {
            return _repository.AddCursDinamic(cursDinamic);
        }


        public int SaveCurrencyList(List<Currency> currencies)
        {
            return _repository.SaveCurrencyList(currencies);
        }


        public bool ClearAllTables()
        {
            return _repository.ClearAllTables();
        }


        public bool ClearTable(string tableName)
        {
            return _repository.ClearTable(tableName);
        }


        public bool CleanByCondition(string tableName, bool redisUsed)
        {
            return _repository.CleanByCondition(tableName, redisUsed);
        }


        public bool DropTables()
        {
            return _repository.DropTables();
        }


        public int GetCurrencyCount()
        {
            return _repository.GetCurrencyCount();
        }


        public int GetCursDinamicCount()
        {
            return _repository.GetCursDinamicCount();
        }


        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        public List<int> GetIdList()
        {
            return _repository.GetIdList();
        }


        public CursDinamic GetRecord(int id)
        {
            return _repository.GetRecord(id);
        }


        public void AddRecord(Auxiliary record)
        {
            _repository.AddRecord(record);
        }


        public void AddTiming(Timing record)
        {
            _repository.AddTiming(record);
        }


        public List<Timing> GetTiming()
        {
            return _repository.GetTiming();
        }


        public int GetTimingCount()
        {
            return _repository.GetTimingCount();
        }
    }
}